####  **EggTime notifications**
*EggTimer is a timer app for cooking eggs. You can start and stop the timer, choose different cooking time intervals..*

In this egg app the following steps have been taken:
- apply notifications to the eggtimer app.
- use channels and importance for the app notifications.
- customize and style the notifications.
- use Firebase Cloud Messaging to send push notifications to your app users to remind them to eat eggs
- subscribe to FCM topics of the FirebaseMessaging class
- whatever app is in background or foreground, notifications will appear and remind user
