package com.nerallan.eggtimernotification.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import com.nerallan.eggtimernotification.R

fun Context.createChannel(channelId: String, channelName: String) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val notificationChannel = NotificationChannel(
            channelId,
            channelName,
            NotificationManager.IMPORTANCE_HIGH
        ).apply {
            setShowBadge(true)
            enableLights(true)
            lightColor = Color.RED
            enableVibration(true)
            description =
                getString(R.string.breakfast_notification_channel_description)
        }

        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        val notificationManager = this.getSystemService(
            NotificationManager::class.java
        )
        notificationManager?.createNotificationChannel(notificationChannel)
    }
}