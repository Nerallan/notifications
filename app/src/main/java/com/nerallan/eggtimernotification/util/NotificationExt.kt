package com.nerallan.eggtimernotification.util

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.core.app.NotificationCompat
import com.nerallan.eggtimernotification.MainActivity
import com.nerallan.eggtimernotification.R
import com.nerallan.eggtimernotification.receiver.SnoozeReceiver


private const val NOTIFICATION_ID = 0 // is needed for updating or canceling the notification
private const val REQUEST_CODE = 0 // need to update or cancel this pending intent
private const val FLAGS = 0

/**
 * Builds and delivers the notification.
 *
 * @param messageBody, notification text
 * @param context, activity context
 */
fun NotificationManager.sendNotification(messageBody: String, context: Context) {
    // Create the content intent for the notification, which launches the activity
    val contentIntent = Intent(context, MainActivity::class.java)
    // create pending intent
    val contentPendingIntent = PendingIntent.getActivity(
        context,
        NOTIFICATION_ID,
        contentIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )

    val eggImage = BitmapFactory.decodeResource(
        context.resources,
        R.drawable.cooked_egg
    )

    // create intent to call SnoozeReceiver by clicking snooze notification action button
    val snoozeIntent = Intent(context, SnoozeReceiver::class.java)
    val snoozePendingIntent: PendingIntent = PendingIntent.getBroadcast(
        context,
        REQUEST_CODE,
        snoozeIntent,
        FLAGS
    )
    val notificationBuilder = NotificationCompat.Builder(
        context,
        context.getString(R.string.egg_notification_channel_id) // verify the notification channel name
    )

        // set title, text and icon to builder
        // minimum amount of data need to set in order to send a notification.
        .setSmallIcon(R.drawable.cooked_egg)
        .setContentTitle(
            context
                .getString(R.string.notification_title)
        )
        .setContentText(messageBody)

        // set content intent
        .setContentIntent(contentPendingIntent)
        .setAutoCancel(true)
        .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_VIBRATE)

        // add style to builder
        .setLargeIcon(eggImage)
        .setStyle(NotificationCompat.BigPictureStyle()
                .bigPicture(eggImage)
                .bigLargeIcon(null))

        // add snooze action
        .addAction(
            R.drawable.egg_icon,
            context.getString(R.string.snooze),
            snoozePendingIntent
        )

        // set priority to support devices running Android 7.1 (API level 25) or lower
        .setPriority(NotificationCompat.PRIORITY_HIGH)
        .build()

    // call notify to send notification
    notify(NOTIFICATION_ID, notificationBuilder)
}

/**
 * Cancels all previous shown notifications.
 *
 */
fun NotificationManager.cancelNotifications() {
    cancelAll()
}
