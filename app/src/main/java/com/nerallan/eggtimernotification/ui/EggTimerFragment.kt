package com.nerallan.eggtimernotification.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.messaging.FirebaseMessaging
import com.nerallan.eggtimernotification.R
import com.nerallan.eggtimernotification.databinding.FragmentEggTimerBinding
import com.nerallan.eggtimernotification.util.createChannel

class EggTimerFragment : Fragment() {

    private val BREAKFAST_TOPIC = "breakfast"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentEggTimerBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_egg_timer, container, false
        )

        val viewModel = ViewModelProvider(this).get(EggTimerViewModel::class.java)

        binding.eggTimerViewModel = viewModel
        binding.lifecycleOwner = this.viewLifecycleOwner

        // call create notification channel for local notifications
        requireActivity().createChannel(
            getString(R.string.egg_notification_channel_id),
            getString(R.string.egg_notification_channel_name)
        )

        // create new notification channel for FCM push notifications
        requireActivity().createChannel(
            getString(R.string.breakfast_notification_channel_id),
            getString(R.string.breakfast_notification_channel_name)
        )

        subscribeBreakfastTopic()

        return binding.root
    }

    private fun subscribeBreakfastTopic() {
        FirebaseMessaging.getInstance().subscribeToTopic(BREAKFAST_TOPIC)
            //  to get notified back from FCM on whether your subscription succeeded or failed.
            .addOnCompleteListener { task ->
                var message = getString(R.string.message_subscribed)
                if (!task.isSuccessful) {
                    message = getString(R.string.message_subscribe_failed)
                }
                showToast(message)
            }
    }

    private fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    companion object {
        fun newInstance() = EggTimerFragment()
    }
}

